package teacher;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Teacher {

    public Teacher() {
    }

    public Teacher(String id, String firstname, String lastname) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
    }

    private String id;
    private String firstname;
    private String lastname;
}

