package teacher;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/teacher/info")
public class TeacherController {


    @GetMapping("/{userId}")
    public UserTeacher getTeachersByUserId(
            @PathVariable("userId") String userId) {

        List<Teacher> userTeacherList =  Arrays.asList(
    new Teacher("1", "Fn1", "Ln1"),
    new Teacher("2", "Fn2", "Ln2"));

        UserTeacher userTeacher = new UserTeacher(userTeacherList);

        return userTeacher;
    }
}
