package teacher;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class UserTeacher {

    private List<Teacher> userTeachers;

    public UserTeacher() {
    }

    public UserTeacher(List<Teacher> userTeachers) {
        this.userTeachers = userTeachers;
    }
}
