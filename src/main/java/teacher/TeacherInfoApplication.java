package teacher;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class TeacherInfoApplication {

	public static void main(String[] args) {
		SpringApplication.run(TeacherInfoApplication.class, args);
	}

}
